object DataModule2: TDataModule2
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 284
  Width = 494
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokemon'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 192
    Top = 40
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 288
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 384
    Top = 40
  end
  object FDQuery2: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinadores')
    Left = 288
    Top = 152
  end
  object DataSource2: TDataSource
    DataSet = FDQuery2
    Left = 384
    Top = 152
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 88
    Top = 40
  end
end
