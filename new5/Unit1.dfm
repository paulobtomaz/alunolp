object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 357
  ClientWidth = 616
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 217
    Height = 329
    KeyField = 'identificador'
    ListField = 'nome'
    ListSource = DataModule2.DataSource1
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 231
    Top = 24
    Width = 377
    Height = 177
    Caption = 'Panel1'
    TabOrder = 1
    object DBText1: TDBText
      Left = 41
      Top = 16
      Width = 65
      Height = 17
      DataField = 'identificador'
      DataSource = DataModule2.DataSource1
    end
    object DBText2: TDBText
      Left = 264
      Top = 16
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule2.DataSource1
    end
    object DBText3: TDBText
      Left = 41
      Top = 56
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule2.DataSource1
    end
    object DBText4: TDBText
      Left = 264
      Top = 56
      Width = 73
      Height = 25
      DataField = 'treinador'
      DataSource = DataModule2.DataSource1
    end
  end
end
