<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Barra de Navegação</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>

@media(min-width:1000px){
.espaco_menu{
  margin-left:100px;
}
.espaco_menu li{
  margin-left:40px;
}}

.atividade {
	border-style: solid;
	border-width: 1px;
	border-color: blue;
	padding: 5px;
	border-radius: 10px;
	background-color: aqua;
}

.fundo{
  background-color: #ffc107;
}

.fundo_card{
   background-color: #DCDCDC;
}
#navbarSite a{
  color:white;
  font-size: 20px;
  font-family: Courier;
  font-weight: bold;
}

#navbarSite a:hover{
  color:black;
}
#situacao h4{
  text-align:center;
}
#situacao p{
  text-align:justify;
  text-indent:30px;
}

#menu label{
  font-size:20px;
  font-weight:bold;
}

.altura{
height:350px;
width:auto;
}
.cor {
   font-size:18px;
  color:white;
  font-weight: bold;
}

</style>
</head>
<body>


<!-- Inicio - Menu Barra de Navegação -->
<nav class="navbar navbar-dark navbar-expand-md fundo">
<div class="container">
 <a href="#" class="navbar-brand">
  <img src="img/car_trabalho1.png" /> AMA
 </a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
   <span class="navbar-toggler-icon"></span>
 </button>
 <div class="collapse navbar-collapse" id="navbarSite">
 <ul class="navbar-nav espaco_menu">
   <li class="nav-item">
     <a href="#item1" class="nav-link">Home</a>
   </li>
   <li class="nav-item dropdown">
     <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Detalhes</a>
     <div class="dropdown-menu fundo">
       <a href="lista_agenda.php" class="nav-link">Eventos</a>
	   <a href="agenda.html" class="nav-link">Agenda</a>
	<div class="collapse navbar-collapse" id="navbarSite">
 <ul class="navbar-nav espaco_menu">
   <li class="nav-item">
     <a href="login.php" class="nav-link">Login</a>
   </li>
     </div>
   </li>
 </ul>
</div>
 </div>
</nav>


<!-- Pagina Principal -->
	<div class="container" class="principal">
		<div class="row">
			
			<!-- Atividade -->
			<div class="col-md-4 atividade">
			<form action="http://www.educajogos.com.br/jogos-educativos/alfabetizacao/alfabeto-som/">
			<img src="img/94a0e5923ecae8cbba6f2c1bef5a4889.autismo.jpg" / class="img-fluid"><br />
				<h4>Alfabeto Sonoro</h4>
				<p>
				Descrição da atividade
				</p>	
				
				<button class="btn btn-primary">Entrar na Atividade</button>
			</form>
			</div>
			<!-- Fim Atividade -->
			

			
		</div>
	</div>
<!-- Final Pagina Principal -->
</body>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</html>
