<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AMA </title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>

@media(min-width:1000px){
.espaco_menu{
  margin-left:100px;
}
.espaco_menu li{
  margin-left:40px;
}}

.fundo{
  background-color: #ffc107;
}

.fundo_card{
   background-color: #DCDCDC;
}
#navbarSite a{
  color:white;
  font-size: 20px;
  font-family: Courier;
  font-weight: bold;
}

#navbarSite a:hover{
  color:black;
}
#situacao h4{
  text-align:center;
}
#situacao p{
  text-align:justify;
  text-indent:30px;
}

#menu label{
  font-size:20px;
  font-weight:bold;
}

.altura{
height:350px;
width:auto;
}
.cor {
   font-size:18px;
  color:white;
  font-weight: bold;
}

</style>
</head>
<body>
<!-- Inicio - Menu Barra de Navegação -->
<nav class="navbar navbar-dark navbar-expand-md fundo fixed-top ">
<div class="container">
 <a href="#" class="navbar-brand">
  <img src="img/car_trabalho1.png" /> AMA
 </a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
   <span class="navbar-toggler-icon"></span>
 </button>
 <div class="collapse navbar-collapse" id="navbarSite">
 <ul class="navbar-nav espaco_menu">
   <li class="nav-item">
     <a href="?pagina=home" class="nav-link">Home</a>
   </li>
   <li class="nav-item dropdown">
     <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Detalhes</a>
     <div class="dropdown-menu fundo">
       <a href="#situacao" class="dropdown-item">Situação</a>
       <a href="#mudancas" class="dropdown-item">Mudanças</a>
	   <a href="#empregos" class="dropdown-item">Autismo</a>
       <a href="?pagina=eventos" class="dropdown-item">Eventos</a
	<div class="collapse navbar-collapse" id="navbarSite">
 <ul class="navbar-nav espaco_menu">
   <li class="nav-item">
     <a href="?pagina=login" class="nav-link">Login</a>
   </li>
     </div>
 </ul>
</div>
 </div>
</nav>
<?php
if(!isset($_GET['pagina'])){
  include('views/home.php');
}else {
  $var=$pagina=$_GET['pagina'];
  switch ($pagina) {
    case 'home':
        include('views/home.php');
     break;
    case 'eventos':
       include('views/lista_agenda.php');
    break;
    case 'login':
       include('views/login.php');
    break;
   }
}
?>

</body>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</html>
