object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 364
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CaixaTexto: TEdit
    Left = 16
    Top = 8
    Width = 225
    Height = 21
    TabOrder = 0
  end
  object BotaoClicar: TButton
    Left = 16
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = BotaoClicarClick
  end
  object BotaoExcluir: TButton
    Left = 97
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 2
    OnClick = BotaoExcluirClick
  end
  object BotaoLimpar: TButton
    Left = 178
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 3
    OnClick = BotaoLimparClick
  end
  object ListBox1: TListBox
    Left = 320
    Top = 8
    Width = 361
    Height = 337
    ItemHeight = 13
    TabOrder = 4
  end
end
