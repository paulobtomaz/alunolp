unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    CaixaTexto: TEdit;
    BotaoClicar: TButton;
    BotaoExcluir: TButton;
    BotaoLimpar: TButton;
    ListBox1: TListBox;
    procedure BotaoClicarClick(Sender: TObject);
    procedure BotaoExcluirClick(Sender: TObject);
    procedure BotaoLimparClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BotaoClicarClick(Sender: TObject);
begin
 ListaPersonagens.Items.Add(EditNome.Text);
end;

procedure TForm1.BotaoExcluirClick(Sender: TObject);
begin
 ListaPersonagens.DeleteSelected;
end;

procedure TForm1.BotaoLimparClick(Sender: TObject);
begin
 ListaPersonagens.Clear;
end;

end.
